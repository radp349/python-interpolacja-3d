import numpy as np
import matplotlib.pyplot as plt
import scipy.io
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
density = 150
data = scipy.io.loadmat('data_map.mat')['data_map']

x, y = np.meshgrid(np.linspace(np.min(data[:,0]), np.max(data[:,0]), density), np.linspace(np.min(data[:,1]), np.max(data[:,1]), density))
z = np.zeros((density,density))
print("Proszę poczekać...")
for row,i in enumerate(np.linspace(np.min(data[:,1]), np.max(data[:,1]), density)):
    for col, j in enumerate(np.linspace(np.min(data[:,0]), np.max(data[:,0]), density)):
        near = np.intersect1d(np.intersect1d(np.where(data[:,0] >= j - 0.06),
                                              np.where(data[:,0] <= j + 0.06)),
                               np.intersect1d(np.where(data[:,1] >= i - 0.06),
                                              np.where(data[:,1] <= i + 0.06)))
        z[row][col] = np.sum(data[:,2][near]) / len(near)

fig = plt.figure(figsize=(8, 8), dpi=200)
ax = fig.add_subplot(131, projection='3d')
ay = fig.add_subplot(132, projection='3d')
ax.plot_surface(x, y, z, vmin=np.nanmin(z), vmax=np.nanmax(z),cmap=cm.rainbow)
ay.scatter(data[:,0], data[:,1], data[:,2], s=1)

plt.show()