# README

Niniejszy skrypt Pythona jest używany do wizualizacji trójwymiarowych danych, które są załadowane z pliku `.mat` za pomocą biblioteki `scipy.io`. Skrypt wykorzystuje biblioteki `numpy`, `matplotlib.pyplot`, `mpl_toolkits.mplot3d` i `matplotlib.cm` do generowania i wyświetlania 3D plotów.

Oto krótki opis działania skryptu:

## Ładowanie danych

Skrypt zaczyna od wczytania danych z pliku `data_map.mat` za pomocą funkcji `scipy.io.loadmat()`. Plik `.mat` to format pliku używany przez MATLAB, ale może być odczytywany przez inne narzędzia, takie jak Python.

## Tworzenie siatki

Następnie skrypt tworzy siatkę punktów (x, y) za pomocą funkcji `numpy.meshgrid()`. Zakres wartości `x` i `y` jest określony na podstawie minimalnych i maksymalnych wartości w danych wejściowych.

## Obliczanie wartości `z`

Potem skrypt oblicza wartości `z` dla każdego punktu na siatce. Wartość `z` dla danego punktu jest obliczana jako średnia wartości `z` dla punktów danych, które są blisko tego punktu (w granicach określonej odległości).

## Tworzenie wykresu

Na koniec skrypt tworzy trójwymiarowy wykres danych za pomocą `matplotlib`. Skrypt tworzy dwa subwykresy - jeden z nich jest wykresem powierzchniowym (`ax.plot_surface()`), a drugi to wykres punktowy (`ay.scatter()`), oba przedstawiają te same dane.

Podsumowując, ten skrypt to narzędzie do wizualizacji trójwymiarowych danych. Może być użyte do zrozumienia struktury danych i identyfikacji wzorców w danych.
